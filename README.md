### [Moonkin Calculator](https://zebouski.gitlab.io/moonkin-calc/)

### BiS Quick Links
  - [pre-raid BiS (no worldbosses / rank 14)](https://zebouski.gitlab.io/moonkin-calc-turtlewow/?phase=6&raids=false&worldbosses=false&pvprank=1&enemytype=8)
  - [pre-naxx BiS](https://zebouski.gitlab.io/moonkin-calc-turtlewow/?phase=5&raids=true&worldbosses=true&pvprank=14&enemytype=8)
  - [BiS (no worldbosses)](https://zebouski.gitlab.io/moonkin-calc-turtlewow/?phase=6&raids=true&worldbosses=false&pvprank=14)
  - [BiS](https://zebouski.gitlab.io/moonkin-calc-turtlewow/?phase=6&raids=true&worldbosses=true&pvprank=14)

### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn serve
```

#### Compiles and minifies for production
```
yarn build
```

#### Lints and fixes files
```
yarn lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
